import React, { Component } from 'react';

class EditarPost extends Component {

    //ref
    tituloRef =  React.createRef();
    contenidoRef = React.createRef();


    editarPost = (e) => {
        e.preventDefault();
        //leer los refs
        const post = {
            title :this.tituloRef.current.value,
            body : this.contenidoRef.current.value,
            userId: 1,
            id: this.props.post.id
        }
        
        
        //enviar por props o peticion de axios
        this.props.editarPost(post);

    }

    cargarFormulario= () => {

        if(!this.props.post) return null;
        
        const {title,body} = this.props.post;

        return(
            <form onSubmit={this.editarPost}className="col-8">
                <legend className="text-center">Editar post</legend>
                <div className="form-group">
                     <label>Título del post:</label>
                     <input ref={this.tituloRef} className="form-control" type="text" defaultValue={title}></input>
                </div>
                <div className="form-group">
                     <label>Contenido:</label>
                     <textarea ref={this.contenidoRef} className="form-control"  defaultValue={body}></textarea>
                </div>
                <button type="submit" className="btn btn-success">Actualizar</button>
            </form>
        );
    }
    
    render() { 
        
        return ( 
            <React.Fragment>
            {this.cargarFormulario()}
            </React.Fragment>
         );
    }
}
 
export default EditarPost;