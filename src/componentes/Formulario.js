import React, { Component } from 'react';

class Formulario extends Component {

    //ref
    tituloRef =  React.createRef();
    contenidoRef = React.createRef();


    crearPost = (e) => {
        e.preventDefault();
        //leer los refs
        const post = {
            title :this.tituloRef.current.value,
            body : this.contenidoRef.current.value,
            userId: 1
        }

        //enviar por props o peticion de axios
        this.props.crearPost(post);

    }
    
    render() { 
        return ( 
            <form onSubmit={this.crearPost}className="col-8">
                <legend className="text-center">Crear nuevo post</legend>
                <div className="form-group">
                     <label>Título del post:</label>
                     <input ref={this.tituloRef} className="form-control" type="text" placeholder="Título del Post"></input>
                </div>
                <div className="form-group">
                     <label>Contenido:</label>
                     <textarea ref={this.contenidoRef} className="form-control"  placeholder="Contenido"></textarea>
                </div>
                <button type="submit" className="btn btn-success">Crear</button>
            </form>
         );
    }
}
 
export default Formulario;