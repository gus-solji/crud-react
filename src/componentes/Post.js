import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2'

class Post extends Component {
    
    confirmarEliminacion = () => {

         //destructuring
         const {id} = this.props.info;


        Swal.fire({
            title: '¿Estas seguro de eliminar?',
            text: "No podras revertirlo despues de eliminado",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.value) {
                this.props.eliminarPost(id);
                Swal.fire(
                    'Eliminado!',
                    'Tu post ha sido eliminado.',
                    'success'
                )
            }
          })

      
    }

    render() { 
        //destructuring
        const {id,title} = this.props.info;

        return ( 
           <tr>
               <td>{id}</td>
               <td>{title}</td>
               <td>
                   <Link to={`/post/${id}`} className="btn btn-primary">Ver</Link>
                   <Link to={`/editarPost/${id}`} className="btn btn-warning">Editar</Link>
                   <button onClick={this.confirmarEliminacion} type="button" className="btn btn-danger">Eliminar</button>
               </td>
           </tr>
         );
    }
}
 
export default Post;