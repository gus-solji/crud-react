import React , {Component} from 'react';
import {BrowserRouter,Route, Switch} from 'react-router-dom';
import Axios from 'axios';
import Swal from 'sweetalert2'
import Header from './Header';
import Navegacion from './Navegacion';
import Posts from './Posts';
import SinglePost from './SinglePost';
import Formulario from './Formulario';
import EditarPost from './EditarPost';


class Router extends Component {
    
    state = { 
        posts: []
    }

    componentDidMount() {
        this.obtenerPost();
    }
    
    obtenerPost = () => {
        Axios.get(`https://jsonplaceholder.typicode.com/posts`)
            .then(response => {
               this.setState({
                   posts: response.data
               })
            })
    }

    eliminarPost = (id) => {
        Axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`) 
            .then(response => {
                if(response.status === 200){
                    const posts = [...this.state.posts];
                
                    let resultado = posts.filter(post => (
                        post.id !== id
                    ))

                    this.setState({
                        posts: resultado
                    })
                }
            })
    }

    crearPost = (post) => {
        Axios.post(`https://jsonplaceholder.typicode.com/posts`,{post})
            .then(response =>{
               if(response.status === 201){

                    Swal.fire(
                        'Creado!',
                        'El post se ha creado!',
                        'success'
                    )

                   let postId = {id:response.data.id};
                   const nuevoPost = Object.assign({},response.data.post,postId);
                   console.log(nuevoPost);

                   this.setState(prevState => ({
                        posts: [...prevState.posts,nuevoPost]
                   }))
               }
            })
    }

    editarPost = (postActualizado) => {

        const {id} = postActualizado;

        Axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`,{postActualizado})
            .then(response =>{
                if(response.status === 200){

                    Swal.fire(
                        'Post actualizado!',
                        'El post se ha actualizado correctamente!',
                        'success'
                    )

                    let postId = response.data.id;

                    const posts = [...this.state.posts];

                    const postEditar = posts.findIndex(post => postId === post.id);

                    posts[postEditar] = postActualizado;

                    
                    this.setState({
                        posts
                    })
                }
            })

        
    }

    render() { 
        return ( 
             <BrowserRouter>
                <div className="container">
                    <div className="row justify-content-center">
                        <Header></Header>
                        <Navegacion></Navegacion>
                        <Switch>
                            <Route exact path="/" render={() => {
                                console.log(this.state.posts)
                                 return(
                                     <Posts posts={this.state.posts} eliminarPost={this.eliminarPost}></Posts>
                                 )
                            }}></Route>
                            <Route exact path="/post/:postId" render={ (props) =>{
                                let idPost = props.location.pathname.replace('/post/','');
                                 
                                
                                const posts = this.state.posts;

                                let filtro = posts.filter(post => (
                                    
                                     post.id === Number(idPost)
                                ))

                                
                                return(
                                    <SinglePost post={filtro[0]}></SinglePost>
                                )
                            }}></Route>

                            <Route exact path={'/crearPost'} render={() => {
                                
                                 return(
                                     <Formulario crearPost={this.crearPost}></Formulario>
                                 )
                            }}></Route>

                            <Route exact path="/editarPost/:postId" render={ (props) =>{
                                let idPost = props.location.pathname.replace('/editarPost/','');
                                                                 
                                const posts = this.state.posts;

                                let filtro = posts.filter(post => (
                                    
                                     post.id === Number(idPost)
                                ))

                                
                                return(
                                    <EditarPost post={filtro[0]} editarPost={this.editarPost}></EditarPost>
                                )
                            }}></Route>
                        </Switch>
                    </div>
                </div>
             </BrowserRouter>   
        );
    }
}
 
export default Router;